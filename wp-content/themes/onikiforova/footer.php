<?php 
/*
Template Name: footer
*/
 ?>
 
<footer>
	<div class="container">
		<ul class="nav navbar-nav">
		<li><a href="/about_me.html">Обо мне</a></li>              
		<li><a href="/uslugi.html">Услуги</a></li>             
		<li><a href="/otzivi.html">Отзывы</a></li>             
		<li><a href="/obuchenie.html">Обучение</a></li>            
		<li><a href="/news.html">новости</a></li>            
		<li><a href="/video.html">Видео</a></li>
		<li><a href="/stati.html">Статьи</a></li>
		<li><a href="/contact.html">Контакты</a></li>
		</ul>
	</div>
<div class="social-lables pt-20">
<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/fb.png"></a>
<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/vk.png"></a>
<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/twitter.png"></a>
<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/in.png"></a>
<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/yt.png"></a> 
</div>
<div class="social-lables-long pt-20">
	<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lvk.png"></a>
	<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lfb.png"></a>
	<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lok.png"></a>
</div>
<div class="social-lables-long text-center pt-20">
© Ольга Никифорова. Эксперт в построении системы продаж
</div>
<div class="social-lables-long text-center pt-20">
Дизайн и создание сайта - <a href="#">Aliendes.com</a>
</div>
 
</footer>
<script type="text/javascript">
jQuery(window).load(function() {

    $(".navbar-nav > li > a").mouseover(function () { // binding onclick
        if ($(this).parent().hasClass('selected')) {
            $(".navbar-nav .selected div div").slideUp(100); // hiding popups
            $(".navbar-nav .selected").removeClass("selected");
        } else {
            $(".navbar-nav .selected div div").slideUp(100); // hiding popups
            $(".navbar-nav .selected").removeClass("selected");
            if ($(this).next(".subs").length) {
                $(this).parent().addClass("selected"); // display popup
                $(this).next(".subs").children().slideDown(200);
            } 
        }
		
    }); 

	$(".subs").mouseover(function() { $(".wrp2").css('display','block'); });
	$(".subs").mouseout(function() { $(".wrp2").css('display','none'); });

});
</script>
<script>
    $(function(){
        if($('#ta').length){
            blok_height = Number($('#ta').css('height').replace('px',''));
             
            if(blok_height > 130){
               $('#ta').css('max-height','130px');
               $('.read-next').show();  
            } 
       }
    });
    
    $('.read-next').live('click', function(){
        
        if($('#ta').css('max-height') != 'none'){
           $('#ta').css('max-height','');
           $(this).text('Скрыть');
        } else {
           $('#ta').css('max-height','130px');     
           $(this).text('Читать весь отзыв');
        }
        
        return false;
    });
</script>

<div id="myModal-1" class="reveal-modal">
			<h3>Заказать обратный звонок</h3>
			<input type="text" value="" placeholder="Введите ваше имя">
			<input type="text" value="" placeholder="Введите ваш номер телефона">
			<a href="" class="button">Отправить</a>
			<a class="close-reveal-modal">&#215;</a>
		</div>

</body>
</html> 