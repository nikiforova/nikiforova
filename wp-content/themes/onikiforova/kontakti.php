 <?php 
/*
Template Name: Kontakti
*/
get_header(); 
 ?>
 
 
 <div class="about">
	<div class="container">
		<h1>Контакты</h1>
		<div class="row pb-20">
			<div class="col-xs-11 col-sm-11 col-lg-11 col-md-11 ">
			<div class="about-text">
				<p>Любой текст, контакты
	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			</div>
			<br><br>
				<div class="form-modal col-xs-4 col-sm-4 col-lg-4 col-md-4">
					<h3>Отправить сообщение</h3>
					<input type="text" value="" placeholder="Введите ваше имя">
					<input type="text" value="" placeholder="Введите ваш e-mail">
					<input type="text" value="" placeholder="Введите ваш номер телефона">
					<input type="text" value="" placeholder="Введите ваше сообщение">
					<a href="" class="button">Отправить</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div  class="container ">
	<div class="social-lables-long pt-50 pb-50" style="    text-align: center;">
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lvk.png"></a>
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lfb.png"></a>
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lok.png"></a>
	</div>
</div>

<div class="container pb-60" id="podpiska-info">
	<div class="col-xs-11 col-sm-11 col-lg-11 col-md-11 pb-30">
		<div class="podpiska-info-text podpiska1">
			<h3>Удвоение продаж<br/> за 3 дня</h3>
			<p>Гарантия роста прибыли от <span style="font-size:48px;font-weight:bold;">20%</span></p><br/>
			<input type="button" class="btn btn-danger" name="" value="УДВОИТЬ ПРОДАЖИ">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska1.jpg" />
	</div>
	<div class="col-xs-6 col-sm-6 col-lg-6 col-md-6" style="margin-right: 18px;">
		<div class="podpiska-info-text podpiska2"> 
			<p>Дистанционный видео-практикум</p>
			<h3>УПРАВЛЕНИЕ ОТДЕЛОМ ПРОДАЖ</h3>
			<p>Системный подход в управлении продажами<br/>
обеспечит рост от <span style="font-size:18px;font-weight:bold;">20%</span> до <span style="font-size:18px;font-weight:bold;">20%</span>
</p> 
			<input type="button" class="btn btn-danger" name="" value="Смотреть курс">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska2.jpg" />
	</div>
	<div class="col-xs-5 col-sm-5 col-lg-5 col-md-5">
		<div class="podpiska-info-text podpiska3">
			<h3>Как Определить продажник или нет?</h3><br/>
			<p>Проверьте наличие таланта продавца за 15 минут</p>
			<input type="button" class="btn btn-danger" name="" value="Проверить">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska3.jpg" />
	</div>
</div>

<div class="abonnent">
	<div class="container">
		<div class="col-xs-6 col-sm-8 col-lg-6 col-md-6 text-right pt-25 text ">
		Хочу получать новые материалы сайта
		</div>
		<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 pt-20 button-form">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Введите ваш e-mail">
				 <span class="input-group-addon search-icon">Подписаться</span> 
			</div>
		</div>
	</div>
</div>
 
 
 
 
 
 
  <?php get_footer(); ?>