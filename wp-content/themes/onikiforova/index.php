<?php get_header(); ?>
<div class="banner-box">
	<div class="banner">
		<div class="banner-text"><?php if($BanerText=get_field("BanerText")){echo $BanerText;}?></div>
	
	</div>
</div> 
<div class="registration-box">
<div class="registration-text text-center">
	<?php if($NameVebinar=get_field("NameVebinar")){echo $NameVebinar;}?>
	<br>
	<input type="button" class="btn btn-danger" name="" value="Регистрация">
	<br>
	<span><a href="#" class="all-web">Все вебинары</a></span>
</div>
</div>
<div class="info-box">
	<div  class="container ">
		<div class="col-xs-offset-3 col-xs-6 col-sm-offset-0 col-lg-offset-0 col-md-offset-0 col-sm-3 col-lg-3 col-md-3 text-left  "><img src="/wp-content/themes/onikiforova/style/temp_img/info1.png" class="pl-58">
			<p class="title">Владельцу бизнеса</p>
			<div class="description pr-82 pt-6">Построение системного бизнеса.<br> Переход на новый уровень.<br> 
Рост прибыли.</div>
			
<input type="button" value="Смотреть" name="" class="btn btn-danger ml-64">
		</div>
		<div class="col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-0  col-lg-offset-0 col-md-offset-0  col-lg-3 col-md-3 text-center info2">
			<img src="/wp-content/themes/onikiforova/style/temp_img/info2.png" class="info2">
			<p class="title mb-9">Руководителю<br> отдела продаж</p>
			<div class="description pl-20">Создание продающих систем в<br> условиях кризиса.<br> Увеличение объема продаж.</div>
			<input type="button" value="Смотреть" name="" class="btn btn-danger">
		</div>
		<div class="col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-0 col-lg-offset-0 col-md-offset-0 col-lg-3 col-md-3 text-center info3">
			<img src="/wp-content/themes/onikiforova/style/temp_img/info3.png">
			<p class="title">HR-специалисту</p>
			<div class="description">Определение таланта продавца<br> за 15 минут.<br> Развитие  персонала.</div>
			<input type="button" value="Смотреть" name="" class="btn btn-danger">
		</div>
		<div class="col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-0 col-lg-offset-0 col-md-offset-0 col-lg-3 col-md-3 text-center info4">
			<img src="/wp-content/themes/onikiforova/style/temp_img/info4.png">
			<p class="title">Менеджеру<br> по продажам</p>
			<div class="description">Секреты успешных продавцов.<br> Тест «Талант продавца».<br> Повышение дохода.</div>
			<input type="button" value="Смотреть" name="" class="btn btn-danger">
		</div>
	</div>
</div>
<div class="school-box">
	<header>
		<div class="red-flag">ОБУЧЕНИЕ</div>
	Школа построения системы продаж
	</header>
	<div class="text-box">
		<span class="up-case"><strong>Как построить эффективную систему</strong> </span><br>
		<span><strong>для получения выдающихся результатов в продажах.</strong></span><br>
		<span>В основе курса - только проверенные инструменты.</span>
	</div>
	<div class="text-box-black">
		<p>На протяжении 15 лет я тщательно отбирала результативные для российского бизнеса технологии.</p>
		<p>Теперь я готова поделиться своим успешным опытом, а также показать причины неудач в продажах.</p>
	</div>
	<div class="container pt-53"> 
 
		 <div class="item">
			<header>Дистанционный видео-практикум</header>
			<div class="name  ">Управление отделом продаж</div>
			<div class="desc-black" >Цикл из 20 видео-семинаров</div>
			<div class="yt-img"> 
				<img src="/wp-content/themes/onikiforova/style/temp_img/youtube.jpg">
			</div>
			<div class="text-box2">
				<p>Проверено практикой. Гарантия результата. </p>
				<p>Для руководителей управляющих продажами, владельцев бизнеса и начинающих предпринимателей.</p>
				<p><strong>Пройдя курс целиком вы научитесь строить системные продажи в любом бизнесе.</strong></p>
			</div>
			<div class="text-center">
			<a href="#" class="btn btn-danger">Смотреть весь курс</a>
			</div>
		 </div>
		 <div class="item">
			<header>Очный практикум</header>
			<div class="name pt-6">Совершенствуем навыки <br>управления продажами</div>
			<div class="desc-black" >8 очных семинаров и тренингов</div>
			<div class="yt-img"> 
				<img src="/wp-content/themes/onikiforova/style/temp_img/youtube.jpg">
			</div>
			<div class="text-box2">
				<p>Проверено практикой. Гарантия результата. </p>
				<p>Для руководителей управляющих продажами, владельцев бизнеса и начинающих предпринимателей.</p>
				<p><strong>Пройдя курс целиком вы научитесь строить системные продажи в любом бизнесе.</strong></p>
			</div>
			<div class="text-center">
			<a href="#" class="btn btn-danger">Смотреть весь курс</a>
			</div>
		 </div>
	</div>
</div>	
<div class="know-more-box">
	<div class="red-flag">тест</div>
	<div class="text container ">
		<h2>Как определить «Продажник» или нет?</h2>
		<p>Этим вопросом часто задаются руководители и менеджеры по персоналу.</p>
		<p>15 минутный тест Э. Шпрангера определит <br>
		<strong>врожденные способности к продажам</strong> или укажет на другие таланты</p>
		<input type="button" value="Узнать больше" name="" class="btn btn-danger">
	</div>
</div>
<div class="aboutMe-box">
<div class="aboutMe">
	<div class="container">
		<div class="col-xs-4 col-xs-offset-3 col-sm-5  col-sm-offset-5  col-lg-6 col-lg-offset-5 col-md-6 col-md-offset-5 pt-15">
		<h2>Обо мне</h2>
		<ul class="small ">
			<li>Эксперт-практик в построении системы продаж. </li>
			<li>Основатель Агентства экспертов по продажам SMART2ru. </li>
			<li>Тьютор программы МВА бизнес-школы Открытого Университета Великобритании. </li>
			<li>Научный руководитель дипломных проектов Международного Института 
			Менеджмента ЛИНК. </li>
			<li>Автор и ведущая 20 дневного практического курса для управленцев и 
			предпринимателей «Технология построения системы управления продажами».</li> 
			<li>Предприниматель, бизнес-консультант, бизнес-коуч</li>
			<li>Провизор, Кандидат Экономических Наук, «MBA Стратегия»</li>
		</ul>
		<ul class="big">
		<li>В продажах <strong>более 15 лет</strong>.</li>
		<li>Управление крупной структурой: <strong>16 филиалов</strong> по России, <strong>1500 агентов</strong> и <strong>300 менеджеров</strong>.</li>
		<li>Рекорд продаж – <strong>6,5 млрд руб/год</strong>.</li>
		<li>Обучила <strong>более 2000 человек</strong>.</li>
		<li><strong>50 руководителей</strong> получили личное наставничество и коучинг.</li>
		<li>Реализовано <strong>свыше 70 проектов</strong> в сфере продаж и развития бизнеса.</li>

		</ul>
		<input type="button" class="btn btn-danger green" name="" value="Читать">
		</div>
	</div>
</div>
</div>
<div class="review">
	<div class="container">
		<h2>Отзывы</h2>
		<div class="row pb-70">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 ">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review1.jpg">
			</div>
			<div class="col-xs-9 col-sm-9 col-lg-9 col-md-9 ">
			<h3>Ирина Киселева</h3>
			<p class="review-info">Директор розничной сети </p>
			<p class="review-info">ООО “Норд Барс”, г.Москва</p>
			<div class="review-text">
				<div style="height:'100%'">
		<div id="ta" style="overflow:hidden">
			Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.
			Плюс возможность общения и личные встречи с вами во время курса.
			Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.
		Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.
			По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.
			Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.
			Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно.
			Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.
			Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.
			Особая благодарность Ольге!
			Спасибо за профессионализм и идею курса такого формата!
			</div>		
					<a class="read-next read-more" style="display:none;" href="#">Читать весь отзыв</a>
				</div>
			
			</div>
			</div>
		</div>
		<div class="row pb-70">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 ">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review2.jpg">
			</div>
			<div class="col-xs-9 col-sm-9 col-lg-9 col-md-9 ">
			<h3>Татьяна Черникова </h3>
			<p class="review-info">Директор колледжа парикмахерского мастерства «МОНЕКО», группа компаний “МОНЕ”</p>
			<p class="review-info">ООО «Монеко», г. Москва</p>
			<div class="review-text">
			<div style="height:'100%'">
		<div id="ta2" style="overflow:hidden">
			Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.
			Плюс возможность общения и личные встречи с вами во время курса.
			Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.
		Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.
			По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.
			Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.
			Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно.
			Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.
			Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.
			Особая благодарность Ольге!
			Спасибо за профессионализм и идею курса такого формата!
			</div>		
					<a class="read-next read-more" style="display:none;" href="#">Читать весь отзыв</a>
				</div>
			</div>
			</div>
		</div>
		
		<div class="row pb-70">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-lg-offset-1 col-md-2 col-md-offset-1 ">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review3.jpg">
			</div>
			<div class="col-xs-9 col-sm-9 col-lg-9 col-md-9 ">
			<h3>Ирина Киселева</h3>
			<p class="review-info">Директор розничной сети </p>
			<p class="review-info">ООО “Норд Барс”, г.Москва</p>
			<div class="review-text">
			<p>Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.</p>
			<p>Плюс возможность общения и личные встречи с вами во время курса.</p>
			<p>Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.</p>
			<p>Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.</p>
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге!</p> 
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
		</div>
	</div>
	<div class="hr"><input type="button" value="Все отзывы" name="" class="btn btn-danger green"></div>
</div>
<div class="clients">
	<div class="container">
		<h2>Мне доверяют</h2>
		<div id="hWrapper">
                        <div id="carouselh">
                             <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/logo-banner1.jpg" /><br />
                                </div>
                            <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/logo-banner2.jpg" /><br />
                                </div>
                            <div>
                               <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/logo-banner3.jpg" /><br />
                              </div>
                            <div>
                               <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/logo-banner4.jpg" /><br />
                             </div>
                        </div>
                    </div>
	</div>
</div>
<div class="news-video">
	<div class="container">
		<div class="row">
			<div class="col-xs-4 col-sm-6 col-lg-4 col-md-4 ">
				<h2>Новости</h2>
				<div class="item">
					<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
						<img src="/wp-content/themes/onikiforova/style/temp_img/news.jpg">
					</div>
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8 news-text">
						<h3>Название новости</h3>
						<time>14.09.15</time>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an </p>
					</div>
				</div>
				<div class="item">
					<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
						<img src="/wp-content/themes/onikiforova/style/temp_img/news.jpg">
					</div>
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8 news-text">
						<h3>Название новости</h3>
						<time>14.09.15</time>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an </p>
					</div>
				</div>
				<div class="item">
					<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
						<img src="/wp-content/themes/onikiforova/style/temp_img/news.jpg">
					</div>
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8 news-text">
						<h3>Название новости</h3>
						<time>14.09.15</time>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an </p>
					</div>
				</div>
				<input type="button" class="btn btn-danger green mt-20" name="" value="Все новости">
			</div>
			<div class="col-xs-4 col-sm-6 col-lg-4 col-md-4 ">
			<h2>Статьи</h2>
				<div class="item">
					<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
						<img src="/wp-content/themes/onikiforova/style/temp_img/article.jpg">
					</div>
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8 news-text">
						<h3>Название новости</h3>
						<time>14.09.15</time>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an </p>
					</div>
				</div><div class="item">
					<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
						<img src="/wp-content/themes/onikiforova/style/temp_img/article.jpg">
					</div>
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8 news-text">
						<h3>Название новости</h3>
						<time>14.09.15</time>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an </p>
					</div>
				</div><div class="item">
					<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
						<img src="/wp-content/themes/onikiforova/style/temp_img/article.jpg">
					</div>
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8 news-text">
						<h3>Название новости</h3>
						<time>14.09.15</time>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an </p>
					</div>
				</div>
				<input type="button" class="btn btn-danger green mt-20" name="" value="Все статьи">
			</div>
			<div class="col-xs-4 col-sm-6 col-lg-4 col-md-4 ">
			<h2>Видео</h2>
				<div class="item">
					<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
						<img src="/wp-content/themes/onikiforova/style/temp_img/video.jpg">
					</div>
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8 news-text">
						<h3>Название новости</h3>
						<time>14.09.15</time>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an </p>
					</div>
				</div><div class="item">
					<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
						<img src="/wp-content/themes/onikiforova/style/temp_img/video.jpg">
					</div>
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8 news-text">
						<h3>Название новости</h3>
						<time>14.09.15</time>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an </p>
					</div>
				</div><div class="item">
					<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
						<img src="/wp-content/themes/onikiforova/style/temp_img/video.jpg">
					</div>
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8 news-text">
						<h3>Название новости</h3>
						<time>14.09.15</time>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an </p>
					</div>
				</div>
				<input type="button" class="btn btn-danger green mt-20" name="" value="Все видео">
			</div>
		</div>
	</div>
</div>
<div class="abonnent">
	<div class="container">
		<div class="col-xs-6 col-sm-8 col-lg-6 col-md-6 text-right pt-25 text ">
		Хочу получать новые материалы сайта
		</div>
		<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 pt-20 button-form">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Введите ваш e-mail">
				 <span class="input-group-addon search-icon">Подписаться</span> 
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>