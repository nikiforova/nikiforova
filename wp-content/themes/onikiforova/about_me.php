<?php 
/*
Template Name: about_me
*/
get_header(); 
 ?>







<div class="about">
	<div class="container">
		<h1>Обо мне</h1>
		<div class="row pb-20">
			<div class="col-xs-4 col-sm-6 col-lg-4 col-md-4 " style="margin-right:30px;">
				<img src="/wp-content/themes/onikiforova/style/temp_img/about_me/obo_mne1.jpg">
			</div>
			<div class="col-xs-7 col-sm-7 col-lg-7 col-md-7 ">
			<h3>Кто я</h3>
			<div class="about-text">
			<p>- Эксперт-практик в построении системы продаж. <br>
			- Основатель Агентства экспертов по продажам SMART2ru. <br>
			- Тьютор программы МВА бизнес-школы Открытого Университета Великобритании. <br>
			- Научный руководитель дипломных проектов Международного Института Менеджмента ЛИНК. <br>
			- Автор и ведущая 20 дневного практического курса для управленцев и предпринимателей «Технология  
			  построения системы управления продажами». <br>
			- Предприниматель, бизнес-консультант, бизнес-коуч<br>
			- Провизор, Кандидат Экономических Наук, магистр делового администрирования «MBA Стратегия»<br>
			</p>
			<h3>Мой опыт</h3>
			<p>- В продажах более 15 лет.<br>
- Развитие продающих структур в международных корпорациях, таких как  AIG, Fortis, GeneraliPPF.<br>
- Создание агентской сети в составе 180 агентов и менеджеров по страхованию жизни. <br>
- Управление масштабным бизнесом: 16 филиалов по России, 300 управленцев, 1500  агентов.<br>
- Наставничество и коучинг более 50 сотрудников, которые смогли занять руководящие позиции. <br>
- Рекорд продаж управляемым мною подразделением составил 6,5 млрд рублей  за год. <br>
- Реализовано свыше 70 проектов в сфере продаж и развития бизнеса.<br>
</p>
			<h3>Мои принципы</h3>
			<p>- Завершать начатое. <br>
- Не останавливаться на достигнутом.<br>
- Сохранять лучшее из того, что уже «нажито».<br>
- Расширять границы возможного.<br>
- Совершенствовать бизнес вдумчиво, без разрушительных последствий. <br>
				</p>
			</div>
			</div>
		</div>
	</div>
</div>

<div class="row pb-50">
	<div class="container">
		<h3 style="color: #3a9f8f;">Дипломы и сертификаты</h3>
		<div id="hWrapper-sertifikat"> 
                        <div id="carouselh2">
                             <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/sertifikat/sertifikat1.jpg" /><br />
                             </div>
                             <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/sertifikat/sertifikat2.jpg" /><br />
                             </div>
                             <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/sertifikat/sertifikat3.jpg" /><br />
                             </div>
                             <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/sertifikat/sertifikat4.jpg" /><br />
                             </div>
                             <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/sertifikat/sertifikat5.jpg" /><br />
                             </div>		
                             <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/sertifikat/sertifikat6.jpg" /><br />
                             </div>							 
                        </div>
                    </div>
	</div>
</div>

<div class="about-box">
	<header><div><img alt="" src="/wp-content/themes/onikiforova/style/temp_img/icon-png-0.png" /><h2>Эксперт в построении системы продаж</h2></div></header>
	<div class="container">
		<div class="text-box">
			- Начало карьеры в продажах - 1997 г.<br />
	- Создана авторская методология построения системы управления продающими подразделениями.<br />
	- Реализовано свыше 70 проектов в сфере продаж и развития бизнеса.<br />
	- Опыт личных продаж - 4 года.<br />
	- Многократный опыт создания с «0» продающей структуры.  <br />
	- Управленческий стаж более 10 лет. В том числе, найм агентов, менеджеров, директоров по продажам, директоров филиалов.  Их обучение, наставничество,  планирование, контроль, мотивация<br />
		</div>
	</div>
</div>

<div class="clients pb-60">
	<div class="container">
		<h3 style="color: #3a9f8f;text-align:left;">Кейсы</h3>
		<div id="hWrapper">
                        <div id="carouselh">
                             <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/logo-banner1.jpg" /><br />
                                </div>
                            <div>
                                <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/logo-banner2.jpg" /><br />
                                </div>
                            <div>
                               <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/logo-banner3.jpg" /><br />
                              </div>
                            <div>
                               <img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/logo-banner4.jpg" /><br />
                             </div>
                        </div>
                    </div>

	</div>	<a href="#" class="read-more">Все кейсы</a>
</div>

<div class="about-box box-info-2">
	<header><div><img alt="" src="/wp-content/themes/onikiforova/style/temp_img/icon-png-1.png" /><h2>Автор и ведущая школы построения системы продаж</h2></div></header>
	<div class="container"><div class="text-box">
	<p>Курс состоит из 4 тематических модулей, в каждом из которых по 5 занятий. Итого 20 видео-семинаров.
	В основе практикума лежит уникальная технология построения системы управления продажами, которую я использую в работе. В него включены только те модели, которые привели к успеху, только лучшие методики, позволяющие достигать выдающихся результатов. Все они были проверены в реальных организациях.
	Это целостный, тщательно спланированный цикл обучения с раздаточным материалами, практическими заданиями, видео-записями занятий.</p><br />
		<div class="col-xs-12">
			<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
				<img src="/wp-content/themes/onikiforova/style/temp_img/youtube.jpg">
			</div>
			<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 ">
				<img src="/wp-content/themes/onikiforova/style/temp_img/youtube.jpg">
			</div>
		</div>
			<div class="col-xs-12">
			<a href="#" class="dwld-case">Скачать примеры <br/>
			раздаточного материала</a>
			<a href="#" class="read-more">Подробно об обучении</a>
		</div>	
	</div></div>
</div>

<div class="about-box" style="height: 450px;">
	<header><div><img alt="" src="/wp-content/themes/onikiforova/style/temp_img/icon-png-2.png" /><h2>Стратег по развитию продаж</h2></div></header>
	<div class="container">
		<div class="text-box">
				Стратегия – моя страсть. <br/>
		Как только пазл складывается, стратегия проработана, формализована, тут же в бизнесе происходят невероятные изменения в сторону роста. <br/>
		Стратегия – это образ действий, которого придерживается организация в достижении поставленных целей. <br/><br/>
		Чтобы построить свою стратегию продаж, необходимо ответить на следующие вопросы:<br/>
		- Какой продукт продает организация? <br/>
		- Каковы портреты потенциальных клиентов? <br/>
		- Какие каналы коммуникации с клиентами будут наиболее эффективными? <br/>
		- Какие каналы продаж предпочтительны для вашего продукта и ваших потребительских сегментов?<br/> 
		- Какая технология продаж наилучшим образом соответствует выбранным каналам продаж? <br/>
		- Почему клиенты должны купить продукт или услугу именно в вашей компании? <br/>
		</div>
	</div>
</div>

<div class="review about-me">
	<div class="container">
		<h3>Отзывы</h3>
		<div class="row pb-50">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review1.jpg">
			</div>
			<div class="col-xs-9 col-sm-9 col-lg-9 col-md-9  pb-20">
			<h3>Ирина Киселева</h3>
			<p class="review-info">Директор розничной сети </p>
			<p class="review-info">ООО “Норд Барс”, г.Москва</p>
			<div class="review-text">
			<p>Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.</p>
			<p>Плюс возможность общения и личные встречи с вами во время курса.</p>
			<p>Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.</p>
			<p>Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.</p>
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге!</p> 
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
		<div class="col-xs-12">
			<a href="#" class="read-more">Все отзывы</a>
		</div>	
		</div>
	</div>
</div>

<div class="about-box pb-30" style="height: 540px;">
	<header><div><img alt="" src="/wp-content/themes/onikiforova/style/temp_img/icon-png-3.png" /><h2>Наставник по управленческим компетенциям</h2></div></header>
	<div class="container"><div class="text-box">
		Во многих организациях признали важность и необходимость обучения и развития сотрудников. За менеджерами по продажам наблюдают, периодически проводят тренинги. Чего не скажешь о руководителях подразделений продаж. <br/>
Не часто встречаю организации, где ставят управленческие компетенции, отрабатывают навыки руководства коллективами продаж.<br/><br/>
	
Практика показывает, что 7 из 10 руководителей не знают: <br/>
- Как эффективно проводить планерки.<br/>
- Каким образом  вести индивидуальную встречу с менеджером, не принимая на свои плечи всех его «обезьян».<br/>
- Каковы правила подготовки, проведения,  разбора полевого тренинга.<br/>
- Как проводить обучающие мероприятия: ролевые игры, короткие семинары.<br/>
- Каковы стандарты аттестации менеджеров.<br/>
- И многое другое.<br/><br/>

Каждый из перечисленных видов деятельности  руководителя имеет свою технологию, которая приводит к наилучшему результату. Однако мало кто владеет этими технологиями, чаще работа ведется интуитивно.  <br/>
Согласитесь, грамотное управление персоналом – это 50% успеха компании.<br/>
Мне легко дается не просто обучать, а именно развивать навыки работникам.<br/>

	</div>
</div>
</div>
<div class="review about-me">
	<div class="container">
		<h3>Отзывы</h3>
		<div class="row pb-50">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review1.jpg">
			</div>
			<div class="col-xs-9 col-sm-9 col-lg-9 col-md-9  pb-20">
			<h3>Ирина Киселева</h3>
			<p class="review-info">Директор розничной сети </p>
			<p class="review-info">ООО “Норд Барс”, г.Москва</p>
			<div class="review-text">
			<p>Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.</p>
			<p>Плюс возможность общения и личные встречи с вами во время курса.</p>
			<p>Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.</p>
			<p>Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.</p>
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге!</p> 
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
					<div class="col-xs-12">
			<a href="#" class="read-more">Все отзывы</a>
		</div>	
		</div>
	</div>
</div>

<div class="about-box pb-30" style="height: 356px;">
	<header><div><img alt="" src="/wp-content/themes/onikiforova/style/temp_img/icon-png-4.png" /><h2>Основатель Агентства экспертов по продажам</h2></div></header>
			<div class="container">
		<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2" style="    margin-right: 20px;">
			<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/smart2.jpg" />
		</div>
		<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8">
			<div class="text-box" style="font-size:14px;">
Выполнять масштабные проекты самостоятельно очень сложно. Чтобы проект состоялся, все работы были выполнены качественно и в срок,  клиент остался доволен - нужна экспертная команда. Так возникла идея объединить моих партнеров по различным проектам, независимых бизнес-консультантов под эгидой Агентства экспертов по продажам. 
Начиная с 2011 года я возглавляю агентство, где можно подобрать независимого консультанта по вопросам развития бизнеса. 
<br/><br/>В нашей команде только профессиональные эксперты-практики с большим опытом работы в продажах. Мы находим индивидуальное решение для каждого клиента, учитывая ценовые возможности без ущерба в качестве работ.

			</div>
		</div>	
		</div>	
</div>

<div class="about-f-box">
	<div class="about-f-box-text"> 
		<input type="button" class="btn btn-danger" name="" value="Связаться со мной">
	</div>
</div>

<div  class="container ">
	<div class="social-lables-long pt-50 pb-50" style="    text-align: center;">
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lvk.png"></a>
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lfb.png"></a>
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lok.png"></a>
	</div>
</div>

<div class="container pb-60" id="podpiska-info">
	<div class="col-xs-11 col-sm-11 col-lg-11 col-md-11 pb-30">
		<div class="podpiska-info-text podpiska1">
			<h3>Удвоение продаж<br/> за 3 дня</h3>
			<p>Гарантия роста прибыли от <span style="font-size:48px;font-weight:bold;">20%</span></p><br/>
			<input type="button" class="btn btn-danger" name="" value="УДВОИТЬ ПРОДАЖИ">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska1.jpg" />
	</div>
	<div class="col-xs-6 col-sm-6 col-lg-6 col-md-6" style="margin-right: 18px;">
		<div class="podpiska-info-text podpiska2"> 
			<p>Дистанционный видео-практикум</p>
			<h3>УПРАВЛЕНИЕ ОТДЕЛОМ ПРОДАЖ</h3>
			<p>Системный подход в управлении продажами<br/>
обеспечит рост от <span style="font-size:18px;font-weight:bold;">20%</span> до <span style="font-size:18px;font-weight:bold;">20%</span>
</p> 
			<input type="button" class="btn btn-danger" name="" value="Смотреть курс">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska2.jpg" />
	</div>
	<div class="col-xs-5 col-sm-5 col-lg-5 col-md-5">
		<div class="podpiska-info-text podpiska3">
			<h3>Как Определить продажник или нет?</h3><br/>
			<p>Проверьте наличие таланта продавца за 15 минут</p>
			<input type="button" class="btn btn-danger" name="" value="Проверить">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska3.jpg" />
	</div>
</div>

<div class="abonnent">
	<div class="container">
		<div class="col-xs-6 col-sm-8 col-lg-6 col-md-6 text-right pt-25 text ">
		Хочу получать новые материалы сайта
		</div>
		<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 pt-20 button-form">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Введите ваш e-mail">
				 <span class="input-group-addon search-icon">Подписаться</span> 
			</div>
		</div>
	</div>
</div>







<?php get_footer(); ?>