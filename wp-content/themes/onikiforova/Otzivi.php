<?php 
/*
Template Name: Otzov
*/
get_header(); 
 ?>
<div class="about">
	<div class="container">
		<h1>Отзывы</h1>
		
	</div>
</div>

<div class="review">
	<div class="container">
		<div class="row pb-40">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2 ">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review1.jpg">
			</div>
			<div class="col-xs-10 col-sm-10 col-lg-10 col-md-10 ">
			<h3>Ирина Киселева</h3>
			<p class="review-info">Директор розничной сети </p>
			<p class="review-info">ООО “Норд Барс”, г.Москва</p>
			<div class="review-text">
			<p>Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.</p>
			<p>Плюс возможность общения и личные встречи с вами во время курса.</p>
			<p>Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.</p>
			<p>Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.</p>
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге!</p> 
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
		</div>
		<div class="row pb-40">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review2.jpg">
			</div>
			<div class="col-xs-10 col-sm-10 col-lg-10 col-md-10 ">
			<h3>Татьяна Черникова </h3>
			<p class="review-info">Директор колледжа парикмахерского мастерства «МОНЕКО», группа компаний “МОНЕ”</p>
			<p class="review-info">ООО «Монеко», г. Москва</p>
			<div class="review-text">
			<p>Я обучалась у Ольги Никифоровоaй на вебинарах «Система управления продажами»</p>
			<p>На данном обучении мне крайне полезно было узнать:</p>
			<ul>
			<li>Стратегию продаж</li>
			<li>Систему мотивации продавцов</li>
			</ul>
			<p>На сегодняшний день я начала внедрять у себя в колледже систему мотивации продавцов. И это очень помогло мне в создании KPI.</p>
			<p>После вебинаров, я поняла, что в первую очередь нужно заняться разработкой системы мотивации продавцов.</p>
			<p>Я очень рекомендую данный курс руководителям, которые только начали построение отдела продаж. Это очень упростит поставленную задачу, и поможет избежать многих ошибок.</p>
			<p>Благодарю Ольгу за столь полезный курс.</p>
			
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге! </p>
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
		</div>
		
		<div class="row pb-40">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2 ">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review3.jpg">
			</div>
			<div class="col-xs-10 col-sm-10 col-lg-10 col-md-10 ">
			<h3>Ирина Киселева</h3>
			<p class="review-info">Директор розничной сети </p>
			<p class="review-info">ООО “Норд Барс”, г.Москва</p>
			<div class="review-text">
			<p>Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.</p>
			<p>Плюс возможность общения и личные встречи с вами во время курса.</p>
			<p>Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.</p>
			<p>Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.</p>
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге!</p> 
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
		</div>
		<div class="row pb-40">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2 ">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review1.jpg">
			</div>
			<div class="col-xs-10 col-sm-10 col-lg-10 col-md-10 ">
			<h3>Ирина Киселева</h3>
			<p class="review-info">Директор розничной сети </p>
			<p class="review-info">ООО “Норд Барс”, г.Москва</p>
			<div class="review-text">
			<p>Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.</p>
			<p>Плюс возможность общения и личные встречи с вами во время курса.</p>
			<p>Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.</p>
			<p>Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.</p>
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге!</p> 
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
		</div>
		<div class="row pb-40">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review2.jpg">
			</div>
			<div class="col-xs-10 col-sm-10 col-lg-10 col-md-10 ">
			<h3>Татьяна Черникова </h3>
			<p class="review-info">Директор колледжа парикмахерского мастерства «МОНЕКО», группа компаний “МОНЕ”</p>
			<p class="review-info">ООО «Монеко», г. Москва</p>
			<div class="review-text">
			<p>Я обучалась у Ольги Никифоровоaй на вебинарах «Система управления продажами»</p>
			<p>На данном обучении мне крайне полезно было узнать:</p>
			<ul>
			<li>Стратегию продаж</li>
			<li>Систему мотивации продавцов</li>
			</ul>
			<p>На сегодняшний день я начала внедрять у себя в колледже систему мотивации продавцов. И это очень помогло мне в создании KPI.</p>
			<p>После вебинаров, я поняла, что в первую очередь нужно заняться разработкой системы мотивации продавцов.</p>
			<p>Я очень рекомендую данный курс руководителям, которые только начали построение отдела продаж. Это очень упростит поставленную задачу, и поможет избежать многих ошибок.</p>
			<p>Благодарю Ольгу за столь полезный курс.</p>
			
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге! </p>
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
		</div>
		
		<div class="row pb-40">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2 ">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review3.jpg">
			</div>
			<div class="col-xs-10 col-sm-10 col-lg-10 col-md-10 ">
			<h3>Ирина Киселева</h3>
			<p class="review-info">Директор розничной сети </p>
			<p class="review-info">ООО “Норд Барс”, г.Москва</p>
			<div class="review-text">
			<p>Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.</p>
			<p>Плюс возможность общения и личные встречи с вами во время курса.</p>
			<p>Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.</p>
			<p>Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.</p>
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге!</p> 
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
		</div>
		<div class="row pb-40">
			<div class="col-xs-2 col-sm-2 col-lg-2 col-md-2 ">
			<img src="/wp-content/themes/onikiforova/style/temp_img/review1.jpg">
			</div>
			<div class="col-xs-10 col-sm-10 col-lg-10 col-md-10 ">
			<h3>Ирина Киселева</h3>
			<p class="review-info">Директор розничной сети </p>
			<p class="review-info">ООО “Норд Барс”, г.Москва</p>
			<div class="review-text">
			<p>Первое что меня привлекло это дистанционность. С учетом графика моей работы и желания повышать знания я искала что то похожее. Нужна была новая волна и источник вдохновения для изменений на работе. Внедрения новых идей.</p>
			<p>Плюс возможность общения и личные встречи с вами во время курса.</p>
			<p>Приятно и отлично была подготовлены организационная сторона вопроса - четко вовремя и профессионально организованы вебинары.</p>
			<p>Мне бы хотелось получать печатные материалы за день до вебинара,  чтобы была возможность ознакомится с темой заранее. По первой просьбе я их и получала, спасибо Вам за это.</p>
			<p>По полезности всего курса могу сказать, что он был мне крайне полезен и очень вовремя по ситуации в компании и моего желания ее изменить.</p>
			<p>Максимально использовала  тему найма сотрудников, контроля. Наверно поймала себя на мысли что личные встречи и обучение в группе отличается от вебинаров. Не говоря уже о их преимуществах.</p>
			<p>Курс дал мне возможность почувствовать свои силы в том что я знала из курса менеджмента и понять и сделать для себя открытия в инструментах о которых не знала раньше, выстроить их в четкую схему направленную на продажи, разложить и сложить по полкам нынешнюю ситуацию в компании, понять в чем она хромает, выявить и подтянуть то что было в моей компетенции. Посмотреть на действия фирмы со стороны и изнутри было полезно и всегда полезно. </p>
			<p>Пройти курс было моей личной инициативой, всем рекомендую - не важно какую руководящую должность вы занимаете, знания возбуждают инициативу и помогают двигаться вперед и смотреть на вещи критично.</p>
			<p>Я так и сделала. Взяла на себя новый проект и засучила рукава для работы, который принес мне и компании много перемен. Всем советую курс.</p>
			<p>Особая благодарность Ольге!</p> 
			<p>Спасибо за профессионализм и идею курса такого формата!</p>
			</div>
			<a href="#" class="read-more">Читать весь отзыв</a>
			</div>
		</div>
	</div>
</div>

     <div class="pagination">
             <ul>
                 <li class="active"><a href="#">1</a></li>
                 <li><a href="#">2</a></li>
                 <li><a href="#">3</a></li>
                 <li><a href="#">4</a></li>
                </ul>
      </div>

<div class="otziv-block">
	<div class="container">
		<div class="row pb-20">
			<div class="col-xs-11 col-sm-11 col-lg-11 col-md-11 pb-30 ">
				<div class="form-modal col-xs-4 col-sm-4 col-lg-4 col-md-4">
					<h3>Оставить отзыв</h3>
					<div class="col-xs-1 col-sm-1 col-lg-1 col-md-1 pb-20">
						<img src="/wp-content/themes/onikiforova/style/temp_img/icon-profil.jpg">
					</div>
					<div class="col-xs-5 col-sm-5 col-lg-5 col-md-5 col-sm-offset-3 col-lg-offset-3 pt-40">
						<p style="margin-bottom: 15px;">Добавить фото</p>
						<a href="" class="button-upload">Обзор</a>
					</div>
					<input type="text" value="" placeholder="Введите ваше имя">
					<input type="text" value="" placeholder="Введите ваш e-mail">
					<input type="text" value="" placeholder="Введите ваш номер телефона">
					<input type="text" value="" placeholder="Введите ваше сообщение">
					<input type="text" value="" placeholder="Название организации">
					<input type="text" value="" placeholder="Занимаемая должность">
					<textarea type="text" value="" style="width:405px; padding:10px; height:135px;" placeholder="Напишите свой отзыв"></textarea>
					<a href="" class="button">Отправить</a>
					<span>Спасибо, ваш отзыв будет добавлен
после проверки его модератором.</span>
				</div>
			</div>
		</div>
	</div>
</div>


<div  class="container ">
	<div class="social-lables-long pt-50 pb-50" style="    text-align: center;">
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lvk.png"></a>
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lfb.png"></a>
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lok.png"></a>
	</div>
</div>

<div class="container pb-60" id="podpiska-info">
	<div class="col-xs-11 col-sm-11 col-lg-11 col-md-11 pb-30">
		<div class="podpiska-info-text podpiska1">
			<h3>Удвоение продаж<br/> за 3 дня</h3>
			<p>Гарантия роста прибыли от <span style="font-size:48px;font-weight:bold;">20%</span></p><br/>
			<input type="button" class="btn btn-danger" name="" value="УДВОИТЬ ПРОДАЖИ">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska1.jpg" />
	</div>
	<div class="col-xs-6 col-sm-6 col-lg-6 col-md-6" style="margin-right: 18px;">
		<div class="podpiska-info-text podpiska2"> 
			<p>Дистанционный видео-практикум</p>
			<h3>УПРАВЛЕНИЕ ОТДЕЛОМ ПРОДАЖ</h3>
			<p>Системный подход в управлении продажами<br/>
обеспечит рост от <span style="font-size:18px;font-weight:bold;">20%</span> до <span style="font-size:18px;font-weight:bold;">20%</span>
</p> 
			<input type="button" class="btn btn-danger" name="" value="Смотреть курс">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska2.jpg" />
	</div>
	<div class="col-xs-5 col-sm-5 col-lg-5 col-md-5">
		<div class="podpiska-info-text podpiska3">
			<h3>Как Определить продажник или нет?</h3><br/>
			<p>Проверьте наличие таланта продавца за 15 минут</p>
			<input type="button" class="btn btn-danger" name="" value="Проверить">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska3.jpg" />
	</div>
</div>

<div class="abonnent">
	<div class="container">
		<div class="col-xs-6 col-sm-8 col-lg-6 col-md-6 text-right pt-25 text ">
		Хочу получать новые материалы сайта
		</div>
		<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 pt-20 button-form">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Введите ваш e-mail">
				 <span class="input-group-addon search-icon">Подписаться</span> 
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>