 /*function simulateClick(element) {

 var evt = document.createEvent("MouseEvents");

 evt.initMouseEvent("click", true, true, window,

   1, 0, 0, 0, 0, false, false, false, false, 0, null);

 var cb = element;

 var canceled = !cb.dispatchEvent(evt);

	}*/

$(function(){
	
	$('.select-box .select-btn').click(
		function()
		{
			 //simulateClick($('#idselect'));
			//$(this).parent('.select-box').find('.form-control').size = 3;
			$(this).parent('.select-box').find('.form-control').chosen();
		}
	);
	
	$('.left-menu li a').click(
		function(){
			$(this).parent().find('ul').toggle(100);
			if(!$(this).parent().hasClass('all-categories'))
			{
				if($(this).parent().hasClass('active') ){
					$(this).parent().removeClass('active');
				}else{
					$(this).parent().addClass('active');
				}
			}
			return false;
		}
	);
	
	
	$('.checkbox-inline').click(
		function(){
			if(!$(this).find('.input-checkbox').hasClass('action'))
			{
				$(this).find('.input-checkbox').addClass('action');
				$(this).find('input').val('1');
			}
			else
			{
				$(this).find('.input-checkbox').removeClass('action');
				$(this).find('input').val('0');
			}
			
		}
	);
	$('.basket-count .bg-minus, .basket-count .bg-plus').click(
		function()
		{
			val = parseInt($(this).parent().find('.count-value').val());
			
			if($(this).hasClass('bg-minus') && parseInt(val) > 0)
			{
				$(this).parent().find('.count-value').val(val-1);
			}
			if($(this).hasClass('bg-plus')  )
			{
				$(this).parent().find('.count-value').val(val+1) ;
			}
		}
	);
	
	$('.input-checkbox').click(
		function(){
			 
			if(!$(this).hasClass('action'))
			{
				
				$(this).addClass('action');
				
				$(this).parent('.checkbox-inline').find('input').val('1');
			}
			else
			{
				
				$(this).removeClass('action');
				$(this).parent('.checkbox-inline').find('input').val('0');
			}
			
		}
	);
});
