<?php 
/*
Template Name: Obuchenie
*/
get_header(); 
 ?>
 
 
 
 
 <div class="about">
	<div class="container">
		<h1>Обучение</h1>
		
	</div>
</div>


<div class="couch-box about-box pb-70">
	<header><div><img alt="" src="/wp-content/themes/onikiforova/style/temp_img/icon-png-5.png" /><h2>Онлайн-семинары</h2></div></header>
	<div class="container">
		<div class="couch  pb-60">
			<div class="couch-zn">
				<p>Дистанционный онлайн курс Ольги Никифоровой</p>
				<p style="font-size:18px;color:#ea2e27;font-weight: bold;text-transform: uppercase;">Управление  продажами от «А» до «Я»</p>
				<p>Все, что нужно знать и уметь для управления отделом продаж, собрано в одной программе!</p>
				
			</div>
			<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/imgcouch1.jpg" />
			<div class="couch-zn-time" ><span>14 марта - 05 мая 2016</span></div>
			</div>
			<div class="text-box pb-30">
				Онлайн-семинары или другое название – вебинары проводятся через интернет с живым участием спикера и слушателей. </div>
			
		</div>
</div>

<div class="couch-box about-box pb-70">
	<header><div><img alt="" src="/wp-content/themes/onikiforova/tyle/temp_img/icon-png-6.png" /><h2>Тренинги</h2></div></header>
	<div class="container">
		<div class="couch  pb-60 pb-30">
			<div class="couch-zn">
				<p>Управленческие тренинги
от Ольги Никифоровой</p>
				<p style="font-size:18px;color:#ea2e27;font-weight: bold;text-transform: uppercase;">Руководитель
отдела продаж</p>
				<p>Тренируем главные навыки управленцев
в продажах!</p>
				
			</div>
			<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/imgcouch1.jpg" />
			<div class="couch-zn-time" ><span>02-23 апреля 2016</span></div>
			</div>
			<div class="text-box">
				Тренинги подразумевают живое участие. Основная цель тренингов – получение новых знаний, отработка навыков и умений под руководством тренеров.
			</div>
			
		</div>
</div>

<div class="couch-box about-box pb-70">
	<header><div><img alt="" src="/wp-content/themes/onikiforova/style/temp_img/icon-png-7.png" /><h2>Видео-курсы</h2></div></header>
	<div class="container">
		<div class="couch  pb-60 pb-30">
			<div class="couch-zn">
				<p>Дистанционный видео-практикум</p>
				<p style="font-size:18px;color:#ea2e27;font-weight: bold;text-transform: uppercase;">УПРАВЛЕНИЕ
ОТДЕЛОМ ПРОДАЖ</p>
				<p>Системный подход в управлении продажами
обеспечит рост от 20 до 200%.
</p>
				
			</div>
			<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/imgcouch1.jpg" />
			<div class="couch-zn-time" ><span>20 видео-семинаров<br>
40 академических часов<br>
50 образцов документов
</span></div>
			</div>
			
		</div>
</div>
<div class="about-f-box">
	<div class="about-f-box-text"> 
		<input type="button" class="btn btn-danger" name="" value="Связаться со мной">
	</div>
</div>

<div  class="container ">
	<div class="social-lables-long pt-50 pb-50" style="    text-align: center;">
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lvk.png"></a>
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lfb.png"></a>
		<a href="#"><img src="/wp-content/themes/onikiforova/style/temp_img/lok.png"></a>
	</div>
</div>

<div class="container pb-60" id="podpiska-info">
	<div class="col-xs-11 col-sm-11 col-lg-11 col-md-11 pb-30">
		<div class="podpiska-info-text podpiska1">
			<h3>Удвоение продаж<br/> за 3 дня</h3>
			<p>Гарантия роста прибыли от <span style="font-size:48px;font-weight:bold;">20%</span></p><br/>
			<input type="button" class="btn btn-danger" name="" value="УДВОИТЬ ПРОДАЖИ">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska1.jpg" />
	</div>
	<div class="col-xs-6 col-sm-6 col-lg-6 col-md-6" style="margin-right: 18px;">
		<div class="podpiska-info-text podpiska2"> 
			<p>Дистанционный видео-практикум</p>
			<h3>УПРАВЛЕНИЕ ОТДЕЛОМ ПРОДАЖ</h3>
			<p>Системный подход в управлении продажами<br/>
обеспечит рост от <span style="font-size:18px;font-weight:bold;">20%</span> до <span style="font-size:18px;font-weight:bold;">20%</span>
</p> 
			<input type="button" class="btn btn-danger" name="" value="Смотреть курс">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska2.jpg" />
	</div>
	<div class="col-xs-5 col-sm-5 col-lg-5 col-md-5">
		<div class="podpiska-info-text podpiska3">
			<h3>Как Определить продажник или нет?</h3><br/>
			<p>Проверьте наличие таланта продавца за 15 минут</p>
			<input type="button" class="btn btn-danger" name="" value="Проверить">
		</div>
		<img alt="" src="/wp-content/themes/onikiforova/style/temp_img/logo-banner/banner-podpiska3.jpg" />
	</div>
</div>

<div class="abonnent">
	<div class="container">
		<div class="col-xs-6 col-sm-8 col-lg-6 col-md-6 text-right pt-25 text ">
		Хочу получать новые материалы сайта
		</div>
		<div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 pt-20 button-form">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="Введите ваш e-mail">
				 <span class="input-group-addon search-icon">Подписаться</span> 
			</div>
		</div>
	</div>
</div>
 
 <?php get_footer(); ?>