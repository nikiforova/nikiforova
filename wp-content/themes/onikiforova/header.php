<?php 
/*
Template Name: header
*/
 ?>
 
 <!DOCTYPE html>
<html  ><head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php
	wp_title( '|', true, 'right' );
?>
</title>
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <script src="https://yastatic.net/jquery/2.2.0/jquery.min.js" ></script>
 <link rel="stylesheet" href="/wp-content/themes/onikiforova/style/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
 <script src="/wp-content/themes/onikiforova/style/js/bootstrap.min.js"  ></script>
 <link rel="stylesheet" href="/wp-content/themes/onikiforova/style/css/style.css">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/onikiforova/style/js/jquery.reveal.js"></script>
<script type="text/javascript" src="/wp-content/themes/onikiforova/style/js/jscarousel-2.0.0.js"></script>
<link rel="stylesheet" href="/wp-content/themes/onikiforova/style/css/jscarousel-2.0.0.css">
 <script type="text/JavaScript">
$(function(){
    $(window).scroll(function() {
        var top = $(document).scrollTop();
        if (top < 175) $(".nav-collapse").css({top: '0', position: 'relative'});
        else $(".nav-collapse").css({top: '0px', position: 'fixed'});
    });
});
</script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#carouselv').jsCarousel({ onthumbnailclick: function(src) { alert(src); }, autoscroll: true, masked: false, itemstodisplay: 3, orientation: 'v' });
            $('#carouselh').jsCarousel({ onthumbnailclick: function(src) { alert(src); }, autoscroll: false, circular: true, masked: false, itemstodisplay: 4, orientation: 'h' });
            $('#carouselhAuto').jsCarousel({ onthumbnailclick: function(src) { alert(src); }, autoscroll: true, masked: true, itemstodisplay: 4, orientation: 'h' });

        });       
        
    </script>
 	
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="col-xs-12 hidden-sm  hidden-lg hidden-md">
<div class="collapse navbar-collapse "  aria-expanded="true"  id="menu1">
<div class="close">
</div>
<ul class="nav navbar-nav  ">
		<li><a href="/obo-mne">Обо мне</a></li>              
		<li><a href="/uslugi.html">Услуги</a></li>    
		<li><a href="/otzyvy/">Отзывы</a></li>
		<li><a href="/obuchenie/">Обучение</a></li>     
		<li><a href="/novosti/">новости</a></li>  
		<li><a href="/video">Видео</a></li>   
		<li><a href="/stati/">Статьи</a></li>     
		<li><a href="/kontakty/">Контакты</a></li>
</ul>
</div>
</div>
<header>
 
	<button type="button" data-toggle="collapse" data-target="#menu1" class="navbar-toggle collapsed coll-btn" aria-expanded="false">
	<span class="nav-bar-white fa fa-bars"></span> <!--<span class="glyphicon glyphicon-align-justify"></span>-->
	</button>
 
	<div class="container">
		<div class="col-xs-6 col-xs-offset-3 col-sm-5 col-sm-offset-0 col-lg-4 col-lg-offset-0 col-md-4 col-md-offset-0 pt-30">
		<img src="/wp-content/themes/onikiforova/style/temp_img/logo.jpg" class="logo">
		</div>
		<div class="col-xs-10 col-xs-offset-1 col-sm-5 col-sm-offset-0 col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 phone">
			<div class="phone1">+7 (495) 9-517-517</div>     <div class="pl-27 phone2">+7 (905) 705-75-18</div>
			<div class="clearfix"></div>
			<div class="callback" ><a href="#" class="big-link" data-reveal-id="myModal-1" >Заказать обратный звонок</a></div>
		</div>
		<div class="col-xs-8 col-sm-2 col-lg-2 col-md-2 basket-box">
			<div class="basket_img col-xs-8 col-sm-12 col-lg-12 col-md-12"><a href="#" class=" basket"> <strong>Корзина:</strong></a><span class="basket"> 0 руб. (0 товаров)</span></div>
			<div class="col-xs-4 col-sm-12 col-lg-12 col-md-12">
				<div class="row connect pt-20">
					<a href="#"><div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 item text-center mail"> 
					</div></a>
					<a href="#"><div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 item text-center find"> 
					</div></a>
					<a href="#"><div class="col-xs-4 col-sm-4 col-lg-4 col-md-4 item text-center social"> 
					</div></a>
				</div>
			</div>
		</div>
	</div>
	 
		<nav class="nav-collapse" >
			<div class="container">
				<ul class="nav navbar-nav " >
					<li><i class="fa fa-home home"></i></li>  
					<li><a href="/obo-mne">Обо мне</a></li>              
					<li><a href="/uslugi.html">Услуги</a>
						<div class="subs">
                        <div class="wrp2">
                            <ul>
                                <li><a href="#" id="title-menu">&bull; Владельцу</a>
                                    <ul>
                                        <li><a href="#">- Аудит продаж</a></li>
                                        <li><a href="#">- Стратегия продаж</a></li>
                                        <li><a href="#">- Удвоение продаж за 3 месяца</a></li>
                                    </ul>
                                </li>
                                <li><a href="#" id="title-menu">&bull; Руководителю</a>
                                    <ul>
                                        <li><a href="#">- Технология продаж</a></li>
                                        <li><a href="#">- Бизнес-процессы</a></li>
                                        <li><a href="#">- Каналы сбыта</a></li>
                                        <li><a href="#">- Скрипты продаж</a></li>
                                        <li><a href="#">- Воронка продаж</a></li>
										<li><a href="#">- Обучение менеджеров</a></li>
                                        <li><a href="#">- Развитие менеджеров</a></li>
                                        <li><a href="#">- Обучение руководителя продаж</a></li>
                                        <li><a href="#">- Развитие руководителя продаж</a></li>
                                        <li><a href="#">- Контроль продаж</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ul>
                                <li><a href="#" id="title-menu">&bull; Кадровику</a>
                                    <ul>
                                        <li><a href="#">- Поиск менеджеров по продажам</a></li>
                                        <li><a href="#">- Тест - проверка способностей к продажам</a></li>
                                        <li><a href="#">- Система мотивации "Умные продажи"</a></li>
										<li><a href="#">- Должностные инструкции</a></li>
                                    </ul>
                                </li>
                                <li><a href="#" id="title-menu">&bull; Менеджерам</a> 
                                    <ul>
                                        <li><a href="#">- Проверь свой талант продавца - пройди тест</a></li>
                                        <li><a href="#">- Полезные статьи</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
					</li>    
					<li><a href="/otzyvy/">Отзывы</a></li>    
					<li><a href="/obuchenie/">Обучение</a></li>     
					<li><a href="/novosti/">новости</a></li>  
					<li><a href="/video">Видео</a></li>   
					<li><a href="/stati/">Статьи</a></li>     
					<li><a href="/kontakty/">Контакты</a></li>
				</ul>
			</div>
		</nav>
	 
</header>